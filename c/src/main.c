#include <stdio.h>
#include <string.h>

#include "list.h"
#include "stack.h"
#include "queue.h"

void destroy(void *data) {
  if (NULL != data) {
    free(data);
  }
}

void foo() {
  char *l1 = (char *) malloc(8);
  char *l2 = (char *) malloc(8);
  char *l3 = (char *) malloc(8);
  memcpy(l1, "abc", 3);
  memcpy(l2, "CBA", 3);
  memcpy(l3, "123", 3);

  List list;
  list_init(&list, NULL);
  list_ins_next(&list, NULL, l1);
  list_ins_next(&list, NULL, l2);
  list_ins_next(&list, NULL, l3);

  char *xx;
  list_rem_next(&list, NULL, (void **) &xx);
  printf("list: %s\n", xx);
  // do not free xx, because we're still using it

  list_destroy(&list);

  Stack stack;
  stack_init(&stack, NULL);
  stack_push(&stack, l1);
  stack_push(&stack, l2);
  stack_pop(&stack, (void **) &xx);
  printf("stack: %s\n", xx);

  stack_push(&stack, l3);
  stack_pop(&stack, (void **) &xx);
  stack_pop(&stack, (void **) &xx);
  printf("stack: %s\n", xx);
  stack_destroy(&stack);

  Queue queue;
  queue_init(&queue, destroy);
  queue_enqueue(&queue, l1);
  queue_dequeue(&queue, (void **) &xx);
  printf("queue: %s\n", xx);

  queue_enqueue(&queue, l2);
  queue_enqueue(&queue, l3);
  queue_dequeue(&queue, (void **) &xx);
  printf("queue: %s\n", xx);
  queue_dequeue(&queue, (void **) &xx);
  printf("queue: %s\n", xx);
  queue_destroy(&queue);
}

/**
 * Recurse & Tail-Recurse
 */
#include "fact.h"
#include "facttail.h"
#include "factor.h"

void recurse_factorial(int n) {
  int fact_result, facttail_result;
  fact_result = fact(n);
  facttail_result = facttail(n, 1);

  printf("%d! = %d, %d\n", n, fact_result, facttail_result);
}

void tail_recurse_prime_factor(int n) {
  factor(n, n, 2);
}

/**
 * list, clist, dlist
 * queue, stack
 * set
 * chtbl, ohtbl
 * bistree, bitree
 * heap, pqueue
 * graph
 *
 * search, sort
 * nummeths
 * bit, compress
 * encrypt
 * graphalg
 * geometry
 */

int main(void) {
  recurse_factorial(0);
  recurse_factorial(10);
  tail_recurse_prime_factor(128);

//  foo();
  return 0;
}

